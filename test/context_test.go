package test

import (
	"context"
	"fmt"
	"runtime"
	"testing"
	"time"
)

/*
* Context merupakan sebuah data yang membawa value, sinyal cancel, sinyal timeout dan sinyal deadline
* Context biasanya dibuat per request (misal setiap ada request masuk ke server web melalui http request)
* Context digunakan untuk mempermudah meneruskan value dan sinyal antar proses
* Context bersifat immutable (Setelah context dibuat, dia tidak bisa diubah lagi isinya)
* Ketika kita menambahkan value ke dalam context atau menambahkan pengaturan timeout dan sebagainya, Dibelakang secara otomatis akan membuat Child Context baru (turunan) bukan merubah isi context tersebut
* Context menganut konsep parent dan child. Ini mirip sekali dengan konsep Inheritance yang ada pada OOP
 */
func TestContext(t *testing.T) {
	background := context.Background()
	todo := context.TODO()

	fmt.Println(background)
	fmt.Println(todo)
}

func TestContextWithValue(t *testing.T) {
	contextA := context.Background()

	contextB := context.WithValue(contextA, "b", "B")
	contextC := context.WithValue(contextA, "c", "C")

	contextD := context.WithValue(contextB, "d", "D")
	contextE := context.WithValue(contextB, "e", "E")

	contextF := context.WithValue(contextC, "f", "F")

	contextG := context.WithValue(contextF, "g", "G")

	fmt.Println(contextA)
	fmt.Println(contextB)
	fmt.Println(contextC)
	fmt.Println(contextD)
	fmt.Println(contextE)
	fmt.Println(contextF)
	fmt.Println(contextG)

	fmt.Println(contextF.Value("f")) // * Value Sendiri
	fmt.Println(contextF.Value("c")) // * Value Parent
	fmt.Println(contextF.Value("b")) // * Tidak dapat karena tidak ada dalam silsilah hirarkinya
	fmt.Println(contextA.Value("b")) // * Karena contextA Parent, maka dia tidak mengetahui value yang ada pada childnya
}

/*
* Goroutine Leak merupakan kondisi dimana Goroutine jalan terus, tidak pernah menemukan ujungnya dan kita tidak bisa menghentikannya
* Contoh Goroutine Leak
 */
func CreateCounter() chan int {
	destination := make(chan int)

	go func() {
		defer close(destination)

		counter := 1

		for {
			destination <- counter

			counter++
		}
	}()

	return destination
}

func TestGoroutineLeak(t *testing.T) {
	fmt.Println("Total Goroutine Before", runtime.NumGoroutine())

	destination := CreateCounter()

	for n := range destination {
		fmt.Println("Counter", n)

		if n == 10 {
			break
		}
	}

	fmt.Println("Total Goroutine After", runtime.NumGoroutine())
}

func CreateCounterWithContext(ctx context.Context) chan int {
	destination := make(chan int)

	go func() {
		defer close(destination)

		counter := 0

		for {
			select {
			case <-ctx.Done():
				return
			default:
				counter++

				destination <- counter

				time.Sleep(1 * time.Second) // untuk kebutuhan testing context with timeout
			}

		}
	}()

	return destination
}

func TestContextWithCancel(t *testing.T) {
	fmt.Println("Total Goroutine Before:", runtime.NumGoroutine())

	parent := context.Background()
	ctx, cancel := context.WithCancel(parent)

	destination := CreateCounterWithContext(ctx)

	for n := range destination {
		fmt.Println("Counter", n)

		if n == 10 {
			break
		}
	}

	cancel() // mengirim sinyal cancel kepada context

	time.Sleep(2 * time.Second)

	fmt.Println("Total Goroutine After:", runtime.NumGoroutine())
}

func TestContextWithTimeout(t *testing.T) {
	fmt.Println("Total Goroutine Before Execution:", runtime.NumGoroutine())

	parent := context.Background()
	ctx, cancel := context.WithTimeout(parent, 5*time.Second)
	defer cancel() // * Perlu dideklarasikan karena apabila eksekusi program  lebih cepat daripada timeout yang kita set, kita wajib mengirimkan sinyal cancel kepada context terkait

	destination := CreateCounterWithContext(ctx)
	fmt.Println("Total Goroutine During Execution:", runtime.NumGoroutine())

	for n := range destination {
		fmt.Println("Counter", n)
	}

	time.Sleep(2 * time.Second)

	fmt.Println("Total Goroutine After Execution:", runtime.NumGoroutine())
}

func TestContextWithDeadline(t *testing.T) {
	fmt.Println("Total Goroutine Before Execution:", runtime.NumGoroutine())

	var parent context.Context = context.Background()
	ctx, cancel := context.WithDeadline(parent, time.Now().Add(5*time.Second))
	defer cancel()

	destination := CreateCounterWithContext(ctx)
	fmt.Println("Total Goroutine During Execution:", runtime.NumGoroutine())

	for n := range destination {
		fmt.Println("Counter", n)
	}

	time.Sleep(2 * time.Second)

	fmt.Println("Total Goroutine After Execution:", runtime.NumGoroutine())
}
